python-xstatic-angular (1.8.2.2-5) unstable; urgency=medium

  * Removed -O--buildsystem=python_distutils from d/rules.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 11:28:39 +0100

python-xstatic-angular (1.8.2.2-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090646).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 17:04:30 +0100

python-xstatic-angular (1.8.2.2-3) unstable; urgency=medium

  * Cleans properly (Closes: #1045170).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 09:54:04 +0200

python-xstatic-angular (1.8.2.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:04:08 +0200

python-xstatic-angular (1.8.2.2-1) experimental; urgency=medium

  * New upstream release.
  * Re-integrate debianize.patch and depends on libjs-angularjs.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Sep 2022 15:21:23 +0200

python-xstatic-angular (1.5.8.0-5) experimental; urgency=medium

  * Remove debianize.patch and use provided AngularJS 1.5.8.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 May 2021 09:00:08 +0200

python-xstatic-angular (1.5.8.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Fixed Homepage URL.
  * Removed useless overrides.
  * Switch to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 Jan 2021 18:19:13 +0100

python-xstatic-angular (1.5.8.0-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:33:30 +0100

python-xstatic-angular (1.5.8.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * Removed Python 2 support.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Apr 2018 00:40:55 +0200

python-xstatic-angular (1.5.8.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Build-depends on openstack-pkg-tools.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 13:04:14 +0000

python-xstatic-angular (1.5.8.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT to new URL
  * d/copyright: Changed source URL to new one

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed depends version for libjs-angularjs.
  * Using pkgos-dh_auto_install.
  * Fixed debian/copyright ordering and years.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Oct 2017 23:57:34 +0200

python-xstatic-angular (1.3.7.0-2) unstable; urgency=medium

  * Lift upperbound restriction for libjs-angularjs version.
  * Standards-Version: is now 3.9.6.
  * Removed Pre-Depends: on dpkg.
  * Added missing dh-python build-depends.
  * Removed version of python-all build-depends.
  * Now using the system's version of AngularJS.
  * Watch file now using github's tag.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Jul 2015 23:49:23 +0200

python-xstatic-angular (1.3.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Now using embedded version of Angular.

 -- Thomas Goirand <zigo@debian.org>  Tue, 10 Feb 2015 16:32:36 +0100

python-xstatic-angular (1.2.23.1-4) unstable; urgency=medium

  * Fixed wrong BASE_DIR directive.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Sep 2014 23:51:49 +0800

python-xstatic-angular (1.2.23.1-3) unstable; urgency=medium

  * Relaxes version dependency.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Sep 2014 01:23:38 +0800

python-xstatic-angular (1.2.23.1-2) unstable; urgency=medium

  * Fixed wrong libjs-angularjs dependency versions.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Sep 2014 07:27:45 +0000

python-xstatic-angular (1.2.23.1-1) unstable; urgency=medium

  * New version of libjs-angularjs, creating new package for it.
    (Closes: #761033)

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Sep 2014 19:39:39 +0800

python-xstatic-angular (1.2.16.1-2) unstable; urgency=medium

  * Added Google INC as copyright holder.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Sep 2014 22:57:48 +0800

python-xstatic-angular (1.2.16.1-1) unstable; urgency=medium

  * Initial release. (Closes: #757778)

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Aug 2014 08:38:26 +0000
